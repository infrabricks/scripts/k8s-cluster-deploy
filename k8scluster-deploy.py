#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = ["Stephane PAILLET"]
__date__ = "2023-10-12"
__license__ = "GNU GPL v3"
__status__ = "Development"
__version__ = "0.1.4alpha"

import click
import csv
import json
import jmespath
from os import getcwd as pwd
from os import chdir
from os.path import isfile
from subprocess import run
from tabulate import tabulate
from jinja2 import Template
from os import environ as setenv
import tarfile
from requests import get as download

# Vars
script_path = pwd()
inventory_dir = "inventory"
one_inventory_path = script_path + "/" + inventory_dir
deploy_user = "root"
limit = "k8s"
kspray_archive_url = "https://github.com/kubernetes-sigs/kubespray/archive/refs/tags"
kspray_version = "2.23.0"
kspray_local_path = script_path + "/kubespray"
kspray_inv_dir = "mycluster"

invtpl = '''{
  "kube_node": {
    {% for row in result -%}
    "{{ row[0] }}": {
      "ip": "{{ row[1] }}"
    }{{ "," if not loop.last else "" }}
    {% endfor -%}
  },
  "kube_control_plane": {
    {% for row in result[:2] -%}
    "{{ row[0] }}": {
      "ip": "{{ row[1] }}"
    }{{ "," if not loop.last else "" }}
    {% endfor -%}
  },
  "etcd": {
    {% for row in result[:3] -%}
    "{{ row[0] }}": {
      "ip": "{{ row[1] }}"
    }{{ "," if not loop.last else "" }}
    {% endfor -%}
  }
}'''

@click.group()
@click.version_option(__version__)
def cli():
    """Script to deploy a Kubernetes cluster"""
    pass

@click.command()
@click.option('--csvfile', help='csv filename')
def csvdisplay(csvfile):
    """Read and display CSV file"""
    if isfile('{}/{}'.format(script_path, csvfile)):
        try:
            data = load_csv(csvfile)
            head = ['Hostname', 'Label', 'vCPU', 'RAM', 'Disque']
            table_content = []
            for row in data:
                table_content.append([row['hostname'], row['label'], 
                row['vcpu'], row['ram'] + 'GB', row['disk'] + 'GB'])
            click.echo(tabulate(table_content, headers = head, tablefmt="grid"))
        except ValueError:
            click.echo('Something goes wrong')
    else:
        click.echo('No csv selected')

@click.command()
@click.option('--csvfile', help='csv filename')
@click.password_option()
def vmdeploy(csvfile, password):
    """Deploy VMs on OpenNebula hypervisor"""
    if isfile('{}/{}'.format(script_path, csvfile)):
        set_vault(password)
        import_roles()
        #deploy_vms(csvfile)
        create_one_inventory()
    else:
        click.echo('No csv selected')

@click.command()
def k8sdeploy():
    """Deploy K8s cluster.
    You need launch VMs deploy command before"""
    prepare_kubespray()
    create_k8s_inventory()
    deploy_k8s()

def load_csv(csvfile):
    """Read csv and load it in a list of dicts"""
    with open('{}/{}'.format(script_path, csvfile)) as csvfile:
        reader = csv.DictReader(csvfile, fieldnames=['hostname', 
                                'label', 'vcpu', 'ram', 'disk'])
        next(reader)
        data = list(reader)
        return data

def set_vault(password):
	vaultfile = open('{}/.vault'.format(script_path), 'w')
	vaultfile.write(password)

def import_roles():
    """Import Ansible roles"""
    try:
        run(['ansible-galaxy', 
             'install', '-f', '-r', 
             '{}/roles/requirements.yml'.format(script_path)])
        click.echo('Ansible roles imported')
    except IOError:
        click.echo('Something goes wrong, Ansible role import aborted')

def deploy_vms(csvfile):
    """Deploy VMs"""
    try:
        data = load_csv(csvfile)
        for row in data:
            run(['ansible-playbook', '-e', 
                 'one_vm_name="{}" one_vm_labels="{}"\
                 one_vm_vcpu="{}"\
                 one_vm_memory="{} GB"\
                 one_vm_disk_size="{} GB"'.format(row['hostname'], 
                 row['label'], row['vcpu'], row['ram'], 
                 row['disk']), '--vault-password-file=.vault', 
                 '{}/one-vms-deploy.yml'.format(script_path)])
    except IOError:
        click.echo('Something goes wrong, VMs deployment aborted')

def create_one_inventory():
    """Create ONE Ansible inventory"""
    one_hosts = '{}/{}/hosts'.format(script_path, inventory_dir)
    try:
        run(['ansible-inventory', '--list', '--output', one_hosts,
             '-i', '{}/{}/inventory-opennebula.yml'
             .format(script_path, inventory_dir)])
        with open(one_hosts) as f:
            d = json.load(f)
        result = jmespath.search('_meta.hostvars.*[name, ansible_host]', d)
        tpl = Template(invtpl)
        data = tpl.render(result= result)
        invfile = open('{}/{}/tmphosts.json'.format(script_path, inventory_dir), 'w')
        invfile.write(data)
        invfile.close

        click.echo('Open Nebula hosts inventory created')

    except IOError:
        click.echo('Something goes wrong, Ansible inventory creation aborted')

def prepare_kubespray():
    """Clone Kubespray repo and prepare inventory"""
    archive_filename = 'v{}.tar.gz'.format(kspray_version)
    try:
        response = download('{}/{}'.format(kspray_archive_url, archive_filename))
        with open(archive_filename, mode="wb") as file:
            file.write(response.content)
        archfile = tarfile.open(archive_filename) 
        archfile.extractall(script_path)
        archfile.close()
        run(['mv', 'kubespray-' + kspray_version, 'kubespray'])
        run(['cp', '-R', kspray_local_path + '/inventory/sample', 
             '{}/inventory/{}'.format(kspray_local_path, kspray_inv_dir)])
        click.echo('Kubespray downloaded, sample inventory copied')

    except IOError:
        click.echo('Something goes wrong, Kubespray git clone aborted')

def create_k8s_inventory():
    """Create k8s Ansible inventory"""
    try:
        setenv["CONFIG_FILE"] = "{}/inventory/{}/hosts.yml".format(
                kspray_local_path, kspray_inv_dir)
        run(['python3', '{}/contrib/inventory_builder/inventory.py'
            .format(kspray_local_path), 'load', '{}/{}/tmphosts.json'
            .format(script_path, inventory_dir)])
        click.echo('K8s ansible inventory created in {}/inventory/{}'
                   .format(kspray_local_path, kspray_inv_dir))

    except IOError:
        click.echo('Something goes wrong, Kubespray inventory creation aborted')

def deploy_k8s():
    """Install Kubernetes cluster"""
    chdir(kspray_local_path)
    try:
        click.echo('Launch k8s deploy...')
        run(['ansible-playbook', '-i', 'inventory/{}/hosts.yml'.format(kspray_inv_dir), 
             '-e', 'ansible_user="{}"'.format(deploy_user), 'cluster.yml'])

    except IOError:
        click.echo('Something goes wrong, Kubespray cluster deployment aborted')

cli.add_command(csvdisplay)
cli.add_command(vmdeploy)
cli.add_command(k8sdeploy)

if __name__ == "__main__":
    cli()

