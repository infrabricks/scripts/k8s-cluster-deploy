# Kubernetes cluster deploy 

"Quick, ugly and dirty" script to deploy and configure a Kubernetes cluster.

/!\ Work in progress /!\

## Informations

* OS : GNU Linux Debian

## Goal

* deploy VMs on Open Nebula cluster
* install Kubernetes cluster with Kubespray

## Requirements

### Python modules
* [pyone](https://pypi.org/project/pyone/)
* [click](https://pypi.org/project/click/)
* [tabulate](https://pypi.org/project/tabulate/)
* [Jinja2](https://pypi.org/project/Jinja2/)
* [requests](https://pypi.org/project/requests/)

### Ansible
* [ONE VM module in Community General Collection](https://docs.ansible.com/ansible/latest/collections/community/general/one_vm_module.html#ansible-collections-community-general-one-vm-module)
* [OpenNebula inventory in Community General Collection](https://docs.ansible.com/ansible/latest/collections/community/general/opennebula_inventory.html)
* [OpenNebula VMs deploy Ansible role](https://gitlab.mim-libre.fr/infrabricks/ansible/roles/one-vms-deploy)
* [Kubespray](https://github.com/kubernetes-sigs/kubespray)

## Usage

**Note :** it's an early alpha release.

To display csv file content :

```
python k8s-cluster-deploy.py csvdisplay --csvfile csvfilename.csv
```

To start VMs deployment :

```
python k8s-cluster-deploy.py vmdeploy --csvfile csvfilename.csv
```

To start K8s cluster deployment :

```
python k8s-cluster-deploy.py k8sdeploy
```

